import React, { Component } from 'react';

import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import styled from 'styled-components';

import Slider from './Slider'


const Repair = styled.div `
	color: white;
	font-size: 24px;
	font-weight: 700;
	line-height:50px;
	font-size: 40px;

	span {
		font-size: 24px;
		display: block;
		line-height:30px;
	}	
`

const LightText = styled.div `
	color: white;
	font-weight: 300;
	line-height:28px;
	font-size: 18px;
	padding: 36px 0;	
`

const CallBtn = styled.button `
	color: white;
	background-color: red;
	font-weight: 400;
	font-size: 18px;
	padding: 15px 37px;	
	border-radius: 35px;
`

class Main extends React.Component {
	render() {
		return (
				<Row>
					<Col lg={5}>
					<Repair>
						Качественный ремонт <span>iphone за 35 минут и гарантия 1 год</span>
					</Repair>

					<LightText>Оставьте заявку на бесплатную диагностику без очереди,
						и получите защитное стекло, стоимостью 1000 рублей,
						с установкой в подарок!
					</LightText>

					<CallBtn>Отправить заявку!
					</CallBtn>

					</Col>

					<Col lg={6} lgoffset={1}>

					<Slider />
					
					</Col>
				</Row>
			)
	}
}

export default Main