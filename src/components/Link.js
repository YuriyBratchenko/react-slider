import React, { Component } from 'react';
import styled from 'styled-components';

const StyledLink = styled.a`
	color: ${props => props.active ? 'pink' : '#fff'};
	font-size: 14px;
	font-weight: 300;
	line-heigh: 24px;
	padding-right: 50px;
	border-right: 1px dotted white;

	&:hover {
		color: red;
	}

	&:last-child {
		border-right: none;
	}
`

class Link extends React.Component {
	render() {
		return (
				<StyledLink className={"link"} href={this.props.link}> {this.props.text} </StyledLink>
			)
	}
}


export default Link