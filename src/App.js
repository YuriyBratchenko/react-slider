import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import Grid from 'react-bootstrap/lib/Grid';

import styled from 'styled-components';
import Header from './components/Header'
import Menu from './components/Menu'
import Main from './components/Main'

const url = process.env.PUBLIC_URL + '/img/bg.jpg'

const HeaderWrapper = styled.header`
  padding: 20px
  background-color: #242424;
`

const MenuWrapper = styled.div`
  height: 70px;
  opacity: 0.8;
  padding: 13px;
`

const MainWrapper = styled.main`
  height: 600px;
  background: url(${url}) no-repeat;
  background-size: cover;
  padding-top: 130px
`


class App extends Component {
  render() {
    return (
        <div className="App">
          <HeaderWrapper>
            <Grid>
              <Header />
            </Grid>
          </HeaderWrapper>
          <MenuWrapper>
            <Grid>
              <Menu />
            </Grid>
          </MenuWrapper>

          <MainWrapper>
            <Grid>
              <Main />
            </Grid>
          </MainWrapper>
        </div>
    );
  }
}

export default App;
